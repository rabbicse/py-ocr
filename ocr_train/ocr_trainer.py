import cv2
import numpy as np
from itertools import chain
from PIL import Image, ImageFont, ImageDraw, ImageOps
from fontTools.ttLib import TTFont
from fontTools.unicode import Unicode

__author__ = 'rabbi'


class OcrTrainer:
    def __init__(self, ttf, train_output):
        self.ttf = ttf
        self.train_output = train_output

    def get_ttf_chars(self):
        ttf = TTFont(self.ttf, 0, verbose=0, allowVID=0, ignoreDecompileErrors=True, fontNumber=-1)
        chars = chain.from_iterable([y + (Unicode[y[0]],) for y in x.cmap.items()] for x in ttf["cmap"].tables)
        chs = list(chars)
        ttf_chars = [c[0] for c in chs]
        return ttf_chars

    def train_data(self, digitheight):
        """
        create training model based on the given TTF font file
        :param fontfile:
        :param digitheight:
        :return:
        """
        ttf_chars = self.get_ttf_chars()
        ttfont = ImageFont.truetype(self.ttf, digitheight)
        samples = np.empty((0, digitheight * (digitheight / 2)))
        responses = []
        # for n in range(10):
        for n in ttf_chars:
            pil_im = Image.new("RGB", (digitheight, digitheight * 2))
            ImageDraw.Draw(pil_im).text((0, 0), unichr(n), font=ttfont)
            pil_im = pil_im.crop(pil_im.getbbox())
            pil_im = ImageOps.invert(pil_im)
            pil_im.save("train_data/" + str(n) + ".png")

            # convert to cv image
            cv_image = cv2.cvtColor(np.array(pil_im), cv2.COLOR_RGBA2BGRA)
            gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray, (5, 5), 0)
            thresh = cv2.adaptiveThreshold(blur, 255, 1, 1, 11, 2)

            roi = cv2.resize(thresh, (digitheight, digitheight / 2))
            responses.append(n)
            sample = roi.reshape((1, digitheight * (digitheight / 2)))
            samples = np.append(samples, sample, 0)

        samples = np.array(samples, np.float32)
        responses = np.array(responses, np.float32)

        model = cv2.KNearest()
        model.train(samples, responses)

        # save the data
        np.savez(self.train_output, train=samples, train_labels=responses)
        return model