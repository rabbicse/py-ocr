import cv2
import numpy as np
import Image
import ImageFont, ImageDraw
import operator
from ocr_train.ocr_trainer import OcrTrainer

__author__ = 'rabbi'

chars = []
# digit recognition part
def findDigits(imagefile, digitheight, train_data):
    im = cv2.imread(imagefile)
    out = np.zeros(im.shape, np.uint8)
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.adaptiveThreshold(blur, 255, 1, 1, 11, 2)

    contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    cnts = sorted([(c, cv2.boundingRect(c)) for c in contours], key = lambda x: x[1])

    # Now load the data
    train = None
    train_labels = None
    with np.load(train_data) as data:
        train = data['train']
        train_labels = data['train_labels']

    model = cv2.KNearest()
    model.train(train, train_labels)

    xcnts = []

    # model = createDigitsModel(fontfile, digitheight)
    for (cnt, _) in cnts:
        x, y, w, h = cv2.boundingRect(cnt)
        # if h > w and h > (digitheight * 4) / 5 and h < (digitheight * 6) / 5:  # +/-20%
        # if h > 10:
        cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
        roi = thresh[y:y + h, x:x + w]  # crop
        roi = cv2.resize(roi, (digitheight, digitheight / 2))
        roi = roi.reshape((1, digitheight * (digitheight / 2)))
        roi = np.float32(roi)
        retval, results, neigh_resp, dists = model.find_nearest(roi, k=3)
        ci = int((results[0][0]))
        # ch = unichr(ci)
        # print ch
        if ci < 128:
            string = chr(ci)
            chars.append((x, y, string))
            cv2.drawContours(out, [cnt], -1, (0, 255, 255), 1)
            cv2.putText(out, string, (x, y + h), 0, 1, (0, 255, 0))
        else:
            text = unichr(ci)
            chars.append((x, y + h, text))
            # print text
            cv2.drawContours(out, [cnt], -1, (0, 255, 255), 1)
            # cv2.putText(out, text, (x, y + h), 0, 1, (0, 255, 0))

            # image = Image.new("RGB", [320, 320])
            # image = Image.fromarray(out)
            # out_p = Image.fromarray(out)
            # draw = ImageDraw.Draw(out_p)
            # font = ImageFont.truetype("kalpurush.ttf", 18)
            # draw.text((x, y + h), text, (0, 255, 255), font=font)


    out_p = Image.fromarray(out)
    s = ''
    for l in chars:
        s += l[2]
        draw = ImageDraw.Draw(out_p)
        font = ImageFont.truetype("kalpurush.ttf", 20)
        draw.text((l[0], l[1]), l[2], (255, 255, 0), font=font)
    print s

    cv2.imshow('in', im)
    # cv2.imshow('out', out)
    # out_p.show()
    out_m = np.array(out_p)
    cv2.imshow('out', out_m)
    cv2.waitKey(0)
    cv2.destroyWindow('in')
    cv2.destroyWindow('out')


# findDigits('test_bn_up.png', 32)
# print 'done.'
# print chars

## first train with font
# trainer = OcrTrainer('kalpurush.ttf', 'knn_data.npz')
# trainer.train_data(32)

## now test with data
findDigits('test_data/2line.png', 32, 'knn_data.npz')
# newlist = sorted(chars, key=lambda k: k[1])
# newlist1 = sorted(chars, key=lambda k: k[0])
# for l in newlist1:
#     print l